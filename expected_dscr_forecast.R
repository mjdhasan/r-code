########################################################
# Forecastes expected DSCR at i conditional on i-1 
#
########################################################

E.DSCR_Forecast = function(family, # family
                           inputs  # list of requried inputs
){  
  if(is.null(inputs)){ # if no inputs provided use default ones
    cat("Warning: No inputs provided to DSCR_Forecast. Default inputs will be used. \n")
    source(file="inc/dscr_families.R");#   
    inputs = DSCR_Families(family, inputs=inputs);
  }
    
  E.dscr.ti = inputs$E.dscr.ti; sig.dscr.ti = inputs$sig.dscr.ti; lambda = inputs$lambda;
  ti = inputs$ti; mat.debt = inputs$mat.debt; mat.proj = inputs$mat.proj;
  dscr.bc.orig = inputs$dscr.bc.orig; dpay.bc.orig = inputs$dpay.bc.orig;
  mat.debt.orig = which.max(dpay.bc.orig>0); dt = inputs$dt;
  if(family=="rising"){ # these inputs are only defined for the rising family
    mu = inputs$mu;  sigma = inputs$sigma;
  }
  
  source(file="dscr_forecast.R"); 
  dscr.sim = DSCR_Forecast(family,inputs); # obtain one simulation of forecasted dscr
  
  E.dscr = array();
  
  E.dscr[ti] = E.dscr.ti - lambda * sig.dscr.ti;
  
  for(i in (ti+1):mat.proj){    
    if(family=="rising"){
      #       dscr.sim[i] = dscr.sim[i-1]*exp( (mu - lambda*sigma - 0.5 * sigma^2)*1 + sigma * sqrt(1) * z[i] );
      E.dscr[i] = dscr.bc.orig[min(i,mat.debt.orig)]/dscr.bc.orig[min((i-1),mat.debt.orig)]*dscr.sim[(i-1)]*
        exp((mu - lambda*sigma)*dt);
    }    
    if(family=="flat"){
      E.dscr[i] = E.dscr.ti - lambda * sig.dscr.ti;
    }      
  }
  
  E.dscr
  
}

E.DSCR_Forecast(family="rising", inputs=NULL);




