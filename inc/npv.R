######################################################
# Computes NPV with continuous compounding
# at time t0 for 
# an initial investment of I made at time t0, yielding
# cash flows, cf, starting from t0
# discounted at a rate r
######################################################

NPV = function(r,cf,I,t0){
  mat = length(cf); 
  t = c((t0+1):mat);
  # npv.simple = sum(cf[t0:mat]/(1+r)^(t-t0)) - I;
  npv.cont = sum(cf[t]*exp(-r*(t-t0))) - I; 
  npv.cont
}  


