#####################################################
# Checks internal consistency of code blocks
# 
#
#####################################################

source(file="func_list.R");

PMT(rate=0.1, debt.init=100, t0=0, ti=11, tf=20); # 45.22621

PV_Debt(i=0, ti=11, tf=20, rate=0.1, pmt=45.22621); #  99.99999

NPV(r=0.1, cf=c(rep(0,10),rep(45.22621,10)), I=000, t0=0); # 99.99999

IRR(cf=c(rep(0,10),rep(45.22621,10)), I=100, t0=0); #  0.09998548

